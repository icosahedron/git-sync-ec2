#!/bin/sh

status=$(git-sync-ec2-instance-status.sh)

if [[ "$status" != "stopped" && "$status" != "pending" && "$status" != "running" ]]
then
	echo "Unknown status of EC2 instance; exitting";
	exit 1;
fi

name=$(git-sync-ec2-instance-name.sh)
echo "Stopping $name"

instance_id=$(git config --get sync-ec2.instance-id)

while [[ "$status" != "stopped" ]]
do
	echo "Stopping..."
    temp=$(aws ec2 stop-instances --instance-ids $instance_id);
    sleep 10s;
    status=$(git-sync-ec2-instance-status.sh)
done

echo "Stopped."
