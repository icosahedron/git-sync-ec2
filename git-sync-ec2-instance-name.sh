#!/bin/sh

status=$(git-sync-ec2-instance-status.sh)
instance_id=$(git config --get sync-ec2.instance-id)

if [[ "$status" != "running" ]]
then
	echo "Server not running.";
	exit 1;
fi

echo `aws ec2 describe-instances --instance-ids $instance_id | jq ".Reservations[0].Instances[0].PublicDnsName" | sed "s/^\([\"']\)\(.*\)\1\$/\2/g"`
exit 0
